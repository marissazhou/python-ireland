.. _funding:

#######
Funding
#######
Python Ireland will try and support local events if we have the funds.
Please contact us at contact@python.ie

[For PyIE Organisers] Getting funding
=====================================
Here are associations to contact about funding.

PSF
---
There are three ways to get funding:

* Sprint funding - http://pythonsprints.com/
* Outreach and education funding (e.g. intro workshop, diversity workshops) - http://mail.python.org/mailman/listinfo/outreach-and-education
* Other grants, through the main grants program  - http://www.python.org/psf/grants/