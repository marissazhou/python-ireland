.. _meetups:

############
Meetups & PR
############
Python Ireland meetups are held on the 2nd Wednesday monthly regardless if there are talks or not. If it's the latter, then a pub meetup is organised instead.

A few things need to be done:

* Post details to
    * **Twitter** `@PythonIreland <http://twitter.com/pythonireland/>`_ with the hashtag *#PythonIE*
    * **Facebook** `Python Ireland Facebook Group <https://www.facebook.com/groups/20154483464/>`_
        * We have a `Python Ireland Facebook Page <https://www.facebook.com/pages/Python-Ireland/112652892100109?fref=ts>`_ as well, but not used because of lack of time to update.
    * **LinkedIn** `Python Ireland LinkedIn group <http://www.linkedin.com/groups/Python-Ireland-40749?>`_
    * **Lanyrd** `Lanyrd <http://lanyrd.com/>`_
        * Add to *Tech events around Ireland* and *Python events around Ireland* Guides
        * Tag topics with *Python*, *Ireland*, *Dublin*
        * Add location, organisers details and details of the meetup (talk or pub)
        * Claim the event so you have addiuutional admin privileges.
    * **Meetup** `Meetup.com <http://www.meetup.com/pythonireland/>`_
        * Create a new event. [#]_
        * Remember to thank Intercom, our meetup.com sponsor.
    * **Mailing list** Python Announce mailing list
        * python-announce-list@python.org
        * Do make sure you have all the details required for the announcement, see emails from `archives <http://mail.python.org/pipermail/python-announce-list/>`_ as a reference.
    * **Other usergroups**:
        * dublinaltnet@googlegroups.com
        * dublin-game-craft@googlegroups.com
        * dublinjs@googlegroups.com
        * gdg-dublin@googlegroups.com
        * ruby_ireland@googlegroups.com
 
Locations
=========
Following are locations we have had our social meetups and talks.

Cork
----

Social

* Camden Palace Hotel
* Clarion Hotel
* Franciscan Well

Dublin
------

Talks

* BAML
* Amazon
* DogPatch Labs
* Central Hotel
* DIT
* Jury's Inn, Parnell Street
* Science Gallery (studio upstairs)

Social

* Agains the Grain (upstairs and downstairs)
* **Bull and Castle (Beer Hall)** (upstaris are big and provides free chickern wings)
* Karma Stone
* Longstone
* Lord Edward's Bar and Lounge
* Market Bar
* Neary's
* O'Neill's, Pearse Street
* Slattery's Bar & Early House
* **The Black Sheep** Downstairs are good! 
* The Grand Central
* The Schoolhouse Pub
* Trinity Capital Hotel
* Harbar Masters
* Lincon's Inn


Galway
------

* 091Labs
* DERI
* Forster Court pub 
* McSwiggans Bar and Restaurant
* The Galway Arms
* The Westwood bar

Checklist
=========
* Organiser(s) contact number / Twitter name / email
* Sponsors
* Talkers
* Projector (if required) [#]_
* Confirmation of location (if reserved)
* Chequebook, if paying on the night. [#]_
* Receipt from venue/service, if expensing.
* Confirmation with talkers 2 days before

Post-Meetup
===========
* Get slides off presenter (from past experience, speakers tend to only have slides ready right before the talks starts.)
    * Upload them to `Python Ireland's Google Drive <https://drive.google.com/a/python.ie/?tab=mo#folders/0ByawkMkzunElZWFjN2QwOTctNjYyNy00MTNmLThlMjgtN2IwN2NjNWU0ZDBm>`_.
    * Send email, tweet, post on Facebook, LinkedIn, etc. that the slides are up.

Other events
============
Aside from meetups (talks and social), Python Ireland also organises other events, e.g. sprints, unconferences, co-org with other user groups.

Types of Events
---------------
TODO: More info

Funding
=======
If funding is required, please see :ref:`Funding page <funding>` for more information.



====

.. [#] Contact Vicky to add you as co-organiser of an event in meetup.com.
.. [#] Mick and Vicky has a projector, please email them to see if it's available. If a company is happy to sponsor a lend of their projector, that is good also.
.. [#] Ask Vicky.
